# ACME Reservation App

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Make sure REST-API project is running before loading this one
```
http://localhost:8081/
```