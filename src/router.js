import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

let ReserveFolder = "./components/Reservations/"

export default new Router({
  mode: "history",
  routes: [
    // Reservations
    {
      path: "/",
      alias: "/reservations",
      name: "reservations",
      component: () => import(`${ReserveFolder}ReservationsList`)
    },
    {
      path: "/reservations/:id",
      name: "reservation-details",
      component: () => import(`${ReserveFolder}Reservation`)
    },
    {
      path: "/addReservation",
      name: "addReservation",
      component: () => import(`${ReserveFolder}AddReservation`)
    },

    // Tuts
    {
      path: "/tutorials",
      alias: "/tutorials",
      name: "tutorials",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddTutorial")
    }
  ]
});
