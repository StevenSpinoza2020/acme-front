import http from "../http-common";

class RoomDataService {
  getAll() {
    return http.get("/rooms");
  }

  getAvailable() {
    return http.get("/rooms/available");
  }

  get(id) {
    return http.get(`/rooms/${id}`);
  }

}

export default new RoomDataService();
